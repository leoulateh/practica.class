import javax.swing.JOptionPane;

public class MenuPrincipal
{
  public static void main (String arg [])
  {
  String estado;
    char opcion; 
  Computadora computadora=null;
  
  do
  {
    opcion=(JOptionPane.showInputDialog("*********************** Men� principal *******************************\n"+
"a. Crear instancia de computadora sin valores.\n"+
"b. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos.\n"+
"c. Modificar la placa de la computadora.\n"+
"d. Modificar el modelo de la computadora.\n"+
"e. Modificar el tama�o de la pantalla de la computadora.\n"+
"f. Modificar el estado de la computadora.\n"+
"g. Ver los datos de la computadora.\n"+
"h. Salir")).charAt(0);
  
  
  switch (opcion)
  {
    case 'a':
        if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
         computadora= new Computadora();
      break;
      
    case 'b':
      String modelo=JOptionPane.showInputDialog ("Dig�te el modelo de la computadora");
      String placa=JOptionPane.showInputDialog ("Dig�te la placa de la computadora");
      int pantalla=Integer.parseInt (JOptionPane.showInputDialog ("Dig�te el tama�o de la pantalla"));
      computadora= new Computadora (modelo, placa, pantalla);
      JOptionPane.showMessageDialog (null, "Computadora registrada con �xito");
      break;
    case'c':
      if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
          { 
          placa=JOptionPane.showInputDialog ("Dig�te la nueva placa");
          computadora.setPlaca (placa);
          }
      break;
    case'd':
      if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
          { 
          modelo=JOptionPane.showInputDialog ("Dig�te el modelo");
          computadora.setModelo (modelo);
          }
      break;
    case'e':
      if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
           { 
          pantalla=Integer.parseInt (JOptionPane.showInputDialog ("Dig�te las pulgadas de la computadora"));
          computadora.setPantalla (pantalla);
          } 
      break;
    case'f':
      if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
        {
          estado=JOptionPane.showInputDialog ("Dig�te el nuevo estado de la computadora");
          computadora.setEstado (estado);
        }
      break;
    case'g':
      if (computadora==null) 
          JOptionPane.showMessageDialog (null, "No se ha registrado ninguna computadora");
        else
          JOptionPane.showMessageDialog (null,computadora.toString());
      break; 
  }
  }while (opcion!='h');
  }
}
    